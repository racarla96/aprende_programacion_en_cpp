// Basic types

#include <iostream>

using namespace std;

int main(){
	int entero = 15; // Tipo Entero
	float flotante = 10.45; // Tipo Decimal 
	double doublefloat = 16.354646; // Doble de espacio en memoria
	char letra = 'a';
	
	cout << entero << endl;
	cout << flotante << endl;
	cout << doublefloat << endl;
	cout << letra << endl;
	
	return 0;
}
