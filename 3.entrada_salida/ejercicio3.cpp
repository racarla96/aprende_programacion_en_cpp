/*
Escribe un programa que
lea por la entrada est�ndar
los siguientes datos de una persona
-----------------
Campo : Tipo de dato
-----------------
Edad : entero
Sexo : caracter
Altura en metros : float - real
-----------------
muestre los datos en la salida est�ndar 
*/

/*
�Que ocurre si?
Ejecutas este programa
con entradas err�neas.
Observa los resultados.
Por ejemplo, introduzca
un dato de tipo car�cter
cuando se espera un dato
de tipo entero.
*/

#include <iostream>

using namespace std;

int main(){
	
	int edad;
	char sexo[10];
	float altura;
	
	cout << "Introduce la edad: "; cin >> edad;
	cout << "Introduce el sexo: "; cin >> sexo;
	cout << "Introduce la altura: "; cin >> altura;

	cout << endl;
	cout << "Los datos de la pesona son: " << endl;
	cout << "Edad: " << edad << " anyos" << endl;
	cout << "Sexo: " << sexo << endl;
	cout << "Altura: " << altura << " cm" << endl;
	
	return 0;
}
