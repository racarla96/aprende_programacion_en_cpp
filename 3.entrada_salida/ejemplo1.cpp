// Input output

#include <iostream>

using namespace std;

// Los comentarios son muy importantes

// Este tipo de comentario es de una sola linea

/*
Mientras que este comentario sirve
para comentar en bloque
*/

int main(){
	int numero; 
	
	cout << "Introduce un numero: ";
	cin >> numero;
	cout << "El numero es " << numero << endl;
	
	return 0;
}
