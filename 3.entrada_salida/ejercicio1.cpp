/*
Escribe un programa  que lea dos n�meros por la entrada est�ndar y
muestre en la salida est�ndar su suma, resta, multiplicaci�n y divisi�n
*/
#include <iostream>

using namespace std;

int main(){
	
	int n1 = 0, n2 = 0, suma = 0, resta = 0, mult = 0, div = 0;
	
	cout << "Introduce un numero: "; cin >> n1;
	cout << "Introduce otro numero: "; cin >> n2;
	
	suma = n1 + n2;
	resta = n1 - n2;
	mult = n1 * n2;
	div = n1 / n2;
	
	cout << endl;
	cout << "Los resultados son" << endl;
	cout << "Suma: " << suma << endl;
	cout << "Resta: " << resta << endl;
	cout << "Multiplicacion: " << mult << endl;
	cout << "Division: " << div << endl;
	
	return 0;
}
