/*
Escribe un programa que
calcule los resultados de una
ecuacion de segundo grado dando
los coeficientes a, b, c
*/
#include <iostream>
#include <math.h>

using namespace std;

int main(){
	
	float a = 0; 
	float b = 0;
	float c = 0;
	float resultado_1 = 0;
	float resultado_2 = 0;
	
	cout << "Introduce el valor de a: "; cin >> a;
	cout << "Introduce el valor de b: "; cin >> b;
	cout << "Introduce el valor de b: "; cin >> c;

	resultado_1 = (-b + sqrt(pow(b,2) - 4 * a * c))/2*a;
	resultado_2 = (-b - sqrt(pow(b,2) - 4 * a * c))/2*a;
	
	cout.precision(3);

	cout << endl; 
	cout << "Los resultados de la ecuacion de segundo grado son: " << resultado_1 << " y " << resultado_2 << endl;
	
	return 0;
}
/* 
RESULTADO

Introduce el valor de a: 1
Introduce el valor de b: 3
Introduce el valor de b: -4

Los resultados de la ecuacion de segundo grado son: 1 y -4
*/

