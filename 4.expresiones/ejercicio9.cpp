/*
Escribe un programa que
calcule el valor de la siguiente funcion:
f(x,y) = sqrt(x)/y^2-1
dados los valores x e y
*/
#include <iostream>
#include <math.h>

using namespace std;

float function(float x, float y){
	return sqrt(x)/(pow(y,2)-1);
}

int main(){
	
	float x = 0; 
	float y = 0;
	float resultado = 0;
	
	cout << "Introduce el valor de x: "; cin >> x;
	cout << "Introduce el valor de y: "; cin >> y;

	resultado = function(x,y);

	cout.precision(3);

	cout << endl; 
	cout << "El resultado de la funcion es: " << resultado << endl;
	
	return 0;
}
/* 
RESULTADO

Introduce el valor de x: 64
Introduce el valor de y: 2

El resultado de la funcion es: 2.67
*/

