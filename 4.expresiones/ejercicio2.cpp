/*
Escribe un programa que
resuelva la siguiente expresión:
(a+b)/(c+d)
*/
#include <iostream>

using namespace std;

int main(){
	
	// Es buena praxis inicializar las variables
	float a = 0;
	float b = 0;
	float c = 0;
	float d = 0;
	float resultado = 0;
	
	cout << "Introduce el numero a: "; cin >> a;
	cout << "Introduce el numero b: "; cin >> b;
	cout << "Introduce el numero c: "; cin >> c;
	cout << "Introduce el numero d: "; cin >> d;

	resultado = (a+b)/(c+d);
	
	// cout.precision(3);

	cout << endl; 
	cout << "La expresion (a+b)/(c+d): " << resultado << endl;
	
	return 0;
}

/* 
RESULTADOS:

(Sin precision)

Introduce el numero a: 3.45
Introduce el numero b: 2.34
Introduce el numero c: 1.21
Introduce el numero d: 2.12

La expresion (a+b)/(c+d): 1.73874

(Con precision)

Introduce el numero a: 3.45
Introduce el numero b: 2.34
Introduce el numero c: 1.21
Introduce el numero d: 2.12

La expresion (a+b)/(c+d): 1.74


*/

