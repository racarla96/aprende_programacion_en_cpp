/*
Escribe un programa que
calcule la nota final de un estudiante
practicas     : 30%
teoria        : 60%
participacion : 10%
Cada una de las notas sobre 10
*/
#include <iostream>

using namespace std;

int main(){
	
	// Es buena praxis que el
	// nombre de la variable 
	// refleje algun significado
	// de la misma
	float n1 = 0; // n1
	float n2 = 0;
	float n3 = 0;
	float resultado = 0;
	
	cout << "Introduce la nota de practicas: "; cin >> n1;
	cout << "Introduce la nota de teoria: "; cin >> n2;
	cout << "Introduce la nota de participacion: "; cin >> n3;

	resultado = n1 * 0.3 + n2 * 0.6 + n3 * 0.1;
	
	cout.precision(3);

	cout << endl; 
	cout << "La nota final es: " << resultado << endl;
	
	return 0;
}

/* 
RESULTADO

Introduce la nota de practicas: 5
Introduce la nota de teoria: 7
Introduce la nota de participacion: 10

La nota final es: 6.7
*/

