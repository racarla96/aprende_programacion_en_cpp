/*
Escribe un programa que
intercambie los valores de 
dos variables
*/
#include <iostream>

using namespace std;

int main(){
	
	// Es buena praxis inicializar las variables
	float a = 0;
	float b = 0;
	float aux = 0; // auxiliar
	
	cout << "Introduce el numero a: "; cin >> a;
	cout << "Introduce el numero b: "; cin >> b;
	
	cout.precision(3);

	cout << endl;
	cout << "Antes del intercambio" << endl;
	cout << "El valor de a: " << a << endl;
	cout << "El valor de b: " << b << endl;
	
	aux = a;
	a = b;
	b = aux;

	cout << endl;
	cout << "Despues del intercambio" << endl;
	cout << "El valor de a: " << a << endl;
	cout << "El valor de b: " << b << endl;

	
	return 0;
}


