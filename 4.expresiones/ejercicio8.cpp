/*
Escribe un programa que
los catetos de un tri�ngulo rect�ngulo
y obtenga como resultado la hipotenusa
del triangulo.
*/
#include <iostream>
#include <math.h>

using namespace std;

int main(){
	
	float c1 = 0; 
	float c2 = 0;
	float h = 0;
	
	cout << "Introduce el cateto 1: "; cin >> c1;
	cout << "Introduce el cateto 2: "; cin >> c2;

	h = sqrt(pow(c1, 2) + pow(c2, 2));
	
	cout.precision(3);

	cout << endl; 
	cout << "La hipotenusa vale: " << h << endl;
	
	return 0;
}

/* 
RESULTADO

Introduce el cateto 1: 4
Introduce el cateto 2: 3

La hipotenusa vale: 5
*/

