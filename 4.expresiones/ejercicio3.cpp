/*
Escribe un programa que
resuelva la siguiente expresión:
(a+b/c)/(d+e/f)
*/
#include <iostream>

using namespace std;

int main(){
	
	// Es buena praxis inicializar las variables
	float a = 0;
	float b = 0;
	float c = 0;
	float d = 0;
	float e = 0;
	float f = 0;
	float resultado = 0;
	
	cout << "Introduce el numero a: "; cin >> a;
	cout << "Introduce el numero b: "; cin >> b;
	cout << "Introduce el numero c: "; cin >> c;
	cout << "Introduce el numero d: "; cin >> d;
	cout << "Introduce el numero e: "; cin >> e;
	cout << "Introduce el numero f: "; cin >> f;

	resultado = (a+b/c)/(d+e/f);
	
	cout.precision(3);

	cout << endl; 
	cout << "La expresion (a+b/c)/(d+e/f): " << resultado << endl;
	
	return 0;
}

/* 
RESULTADOS:

Introduce el numero a: 12.34
Introduce el numero b: 2.56
Introduce el numero c: 3.45
Introduce el numero d: 8.76
Introduce el numero e: 4.56
Introduce el numero f: 3.21

La expresion (a+b/c)/(d+e/f): 1.29
*/

