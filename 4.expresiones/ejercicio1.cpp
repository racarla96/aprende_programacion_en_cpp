/*
Escribe un programa que
resuelva la siguiente expresión:
a/b + 1
*/
#include <iostream>

using namespace std;

int main(){
	
	float a = 0;
	float b = 0;
	
	cout << "Introduce un numero: "; cin >> a;
	cout << "Introduce otro numero: "; cin >> b;

	cout << endl; 
	cout << "La expresion a/b + 1: " << (a/b)+1 << endl;
	
	/*
	Se recomienda el uso de parentesis en las expressiones
	ya que como podemos observar no es necesario el uso 
 	en esta expresion porque la division siempre va antes
 	que la suma, pero cuando la expression crece, se puede
 	volver confusa la expression (por claridad de la expresion)
	*/
	
	return 0;
}
