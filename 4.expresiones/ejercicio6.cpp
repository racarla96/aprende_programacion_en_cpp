/*
Escribe un programa que
lea la nota de cuatro alumnos
y calcula la nota media de los cuatro
*/
#include <iostream>

using namespace std;

int main(){
	
	// Es buena praxis que el
	// nombre de la variable 
	// refleje algun significado
	// de la misma
	float n1 = 0; // n1 - nota del alumno 1
	float n2 = 0;
	float n3 = 0;
	float n4 = 0;
	float resultado = 0;
	
	cout << "Introduce la nota del alumno 1: "; cin >> n1;
	cout << "Introduce la nota del alumno 2: "; cin >> n2;
	cout << "Introduce la nota del alumno 3: "; cin >> n3;
	cout << "Introduce la nota del alumno 4: "; cin >> n4;

	resultado = (n1 + n2 + n3 + n4) / 4;
	
	cout.precision(3);

	cout << endl; 
	cout << "La media de las notas es: " << resultado << endl;
	
	return 0;
}

/* 
RESULTADOS

Introduce la nota del alumno 1: 14
Introduce la nota del alumno 2: 13.5
Introduce la nota del alumno 3: 16.5
Introduce la nota del alumno 4: 10

La media de las notas es: 13.5

----------------------------------

Introduce la nota del alumno 1: 15.67
Introduce la nota del alumno 2: 3.45
Introduce la nota del alumno 3: 11.13
Introduce la nota del alumno 4: 19.44

La media de las notas es: 12.4
*/

